
import Provider from '../core/Provider';

export default class AppPresenter {

  private view: IAppView | null;
  private provider: typeof Provider;

  constructor(provider: typeof Provider) {
    this.provider = provider;
    this.view = null;
  }

  setView(view: IAppView) {
    this.view = view;
  };

  createCliente(form: any) {
    this.view!.showLoading();

    this.provider.createCliente.execute(form).then(
      (result) => {
        this.view!.setClientId(result.id);
        this.view!.hideLoading();
      },
      (error: Error) => {
        this.view!.showError(error);
      }
    );
  }
}

export interface IAppView {
  showLoading: () => void;

  hideLoading: () => void;

  showError: (error: any) => void;

  setClientId: (id: string) => void;
}