import HttpClient from "../infrastructure/HttpClient";

export default class CreateCliente {
  constructor(private httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  async execute(cliente: any) {
    return this.httpClient.post('https://jsonplaceholder.typicode.com/posts', cliente);
  }
}
