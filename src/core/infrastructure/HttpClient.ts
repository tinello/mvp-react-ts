export default class HttpClient {

  private endpoint: string = '';
  private body: any = {};

  async post(endpoint: string, body: any) {
    this.endpoint = endpoint;
    this.body = body;

    const optionalJsonBody = this.body ? JSON.stringify(this.body) : '';
    return fetch(this.endpoint, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: optionalJsonBody,
    }).then(async (r) => {
      const response = await r.json();
      if (r.status === 500 && response.message === 'InvalidAuthentication') {
        //console.log(500);
      }
      return response;
    });
  }
}
