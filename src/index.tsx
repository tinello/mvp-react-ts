import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './web/App';
import * as serviceWorker from './serviceWorker';
import Provider from './core/Provider';
import AppPresenter from './web/AppPresenter';

const provider = Provider;
const appPresenter = new AppPresenter(provider);


ReactDOM.render(<App presenter={appPresenter} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
